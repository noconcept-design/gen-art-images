// util
function GetRandomFloat(min: number, max: number):number {
  return Math.random() * (max - min) + min;
}

function GetRandomInt(min: number, max: number):number {
  return Math.floor(GetRandomFloat(min, max));
}

function FromPolar(v: number, theta: number) {
  return [v * Math.cos(theta), v * Math.sin(theta)]
}

function ToLuma(r: number, g: number, b: number): number {
  return 0.2126 * r + 0.7152 * g + 0.0722 * b
}

function Clamp(min: number, max: number, value: number): number {
  return value > max ? max : (value < min ? min : value)
}

////
interface ISimObject {
  Update(imageData: ImageData):void;
  Draw(ctx:CanvasRenderingContext2D):void;
}

const MaxParticleSize = 5;

class Particle implements ISimObject {
  x = 0;
  y = 0;
  speed = 0;
  theta = 0;
  radius = 1.0;
  ttl = 500;
  lifetime = 500;
  alpha = 0.8;

  color = 'black';

  constructor(private w: number, private h: number, private palette:string[]) {
    this.reset();
  }

  reset() {
    this.x = GetRandomFloat(0, this.w);
    this.y = GetRandomFloat(0, this.h);

    this.speed = GetRandomFloat(0, 3.0);
    this.theta = GetRandomFloat(0, Math.PI * 2);

    this.radius = GetRandomFloat(0.05, 1.0);
    this.lifetime = this.ttl = GetRandomInt(25, 50);

    this.color = this.palette[GetRandomInt(0, this.palette.length)]; 
    
    this.ttl = this.lifetime = GetRandomInt(25, 50);
  }

  imageComplementLuma(imageData: ImageData):number {
    const p = Math.floor(this.x) + Math.floor(this.y) * imageData.width;
    // image rgba
    const i = Math.floor(p * 4);
    const r = imageData.data[i + 0];
    const g = imageData.data[i + 1];
    const b = imageData.data[i + 2];
    // const a = imageData.data[i + 3];

    const luma = ToLuma(r, g, b);
    const ln = 1 - luma / 255.0;
    return ln;
  }
  
  Update(imageData: ImageData) {
    const ln = this.imageComplementLuma(imageData);
    const lt = (this.lifetime - this.ttl) / this.lifetime;

    this.alpha = lt;

    let dRadius = GetRandomFloat(-MaxParticleSize / 5, MaxParticleSize / 5);
    const dSpeed = GetRandomFloat(-0.21, 0.21);
    const dTheta = GetRandomFloat(-Math.PI / 8, Math.PI / 8);
 
    // compute values
    this.speed += dSpeed;
    this.theta += dTheta;
 
    const [dx, dy] = FromPolar(this.speed, this.theta * ln);

    this.x += dx;
    this.y += dy;
    this.x = Clamp(0, this.w, this.x)
    this.y = Clamp(0, this.h, this.y)

    this.radius += dRadius;    
    this.radius = Clamp(0, MaxParticleSize, this.radius) * ln;

    // lifetime
    this.ttl += -1;
    if (this.ttl === 0) {
      this.reset();
    }
  }

  Draw(ctx: CanvasRenderingContext2D) {
    ctx.save();
    this.experiment2(ctx);
    ctx.restore();
  }
  experiment2(ctx:CanvasRenderingContext2D) {
    ctx.fillStyle = this.color;
    ctx.globalAlpha = this.alpha;
    let circle = new Path2D();
    circle.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
    ctx.fill(circle);
  }

}

const ParticleCount = 500;
const ColorPalettes = [
  ['#cdb4db', '#ffc8dd', '#ffafcc', '#bde0fe', '#a2d2ff'],
  ['#ef476f', '#ffd166', '#06d6a0', '#118ab2', '#073b4c'],
  ['#3e5641', '#a24936', '#d36135', '#282b28', '#83bca9'],
  ['#c9e4ca', '#87bba2', '#55828b', '#3b6064', '#364958'],
  ['#f8f4e3', '#d4cdc3', '#d5d0cd', '#a2a392', '#9a998c'],
  ['#2c363f', '#e75a7c', '#f2f5ea', '#d6dbd2', '#bbc7a4'],
  ['#880d1e', '#dd2d4a', '#f26a8d', '#f49cbb', '#cbeef3'],
  ['#3e442b', '#6a7062', '#8d909b', '#aaadc4', '#d6eeff'],
  ['#e63946', '#f1faee', '#a8dadc', '#457b9d', '#1d3557'],

];
class Simulation implements ISimObject {
  particles: Particle[] = [];
  palette: string[] = [];
  constructor(private width: number, private height: number) { 
    this.palette = ColorPalettes[GetRandomInt(0, ColorPalettes.length)]
    for (let i = 0; i < ParticleCount; i++) {
      this.particles.push(new Particle(this.width, this.height, this.palette))
    }
  }
  Update(imageData:ImageData) {
    this.particles.forEach( p => p.Update(imageData))
  }

  init = false;
  Draw(ctx: CanvasRenderingContext2D) {
    if (!this.init) {
      ctx.fillStyle = this.palette[0];
      ctx.fillRect(0, 0, this.width, this.height);
      this.init = true;
    }


    this.particles.forEach( p => p.Draw(ctx))
  }
}

function createDrawCanvas(imageCtx: CanvasRenderingContext2D, width: number, height: number) {
  const updateFrameRate = 50;
  const renderFrameRate = 50;

  // const canvas = document.getElementById('canvas') as HTMLCanvasElement;
  //const canvas = document.createElement('canvas');
  document.body.appendChild(canvas); 
  const ctx = canvas.getContext('2d');

  canvas.width = width;
  canvas.height = height;
  
  
  if (!canvas) return;
  if (!ctx) return;

  ctx.imageSmoothingEnabled = true;
  ctx.imageSmoothingQuality = 'high';

  const sim = new Simulation(width, height);
  const imageData = imageCtx.getImageData(0, 0, width, height);

  setInterval(() => {
    sim.Update(imageData);
  }, 1000 / updateFrameRate);

  setInterval(() => {
    sim.Draw(ctx)
  }, 1000 / renderFrameRate);  
}

function bootstrapper() {
  const width = window.innerWidth;
  const height = window.innerHeight;

  const imageCanvas = document.createElement('canvas');
  // document.body.appendChild(imageCanvas);
  imageCanvas.width = width;
  imageCanvas.height = height;
  const ctx = imageCanvas.getContext('2d');
  if (!ctx) return;

  // image
  var image = new window.Image()
  if (!image) return;
  image.crossOrigin = 'Anonymous';
  image.onload = (e) => {
    ctx.drawImage(image, 0, 0, width, height);
    createDrawCanvas(ctx, width, height);
  }
  const images = ['../assets/images/tessy.jpg', '../assets/images/zx10r-2004.jpg', '../assets/images/zx10r-logo.jpg', '../assets/images/fb.jpg', '../assets/images/yoji.jpg', '../assets/images/smiley.jpg'];
  image.src = images[GetRandomInt(0, images.length)];

} 
bootstrapper();